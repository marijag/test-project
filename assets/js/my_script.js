// A $( document ).ready() block.
$( document ).ready(function() {
    console.log( "ready!" );

    $(".mobile-menu").click(function() {
      $(".menu").css("display", "block");
    });
    $(".menu span").click(function() {
      $(".menu").css("display", "none");
    });

    $(window).resize(function() {
      if($("html").width() > 767) {
        $(".menu").css("display", "inline-block");
      }
      else {
        $(".menu").css("display", "none");
      }
    })


    // Comments

    var comments_counter = 0;

    $("#write_comment_box .btn-send").click(function() {

      if( $("#txt_write_comment").val() != "" && $("#txt_write_comment").val() !="Ooh man! Just write your comment here!") {

        // Post the comment on the page
        var html_text = "";
        html_text += '<div class="comment_box">';
        html_text += '<div class="single_comment">';
        html_text += '<div class="comment_header">';
        html_text += '<img src="assets/images/avatar/avatar-1.png" />';
        html_text += '<div class="name_and_time">';
        html_text += '<div class="name">Alfred Henderson</div>';
        html_text += '<div class="time">4 days ago</div></div>';  // end of name_and_time
        html_text += '<div class="btn-replay"></div></div>';      //end of comment_header
        html_text += '<div class="comment_text">';
        html_text += $("#txt_write_comment").val();
        html_text += '</div>';
        html_text += '</div>';   //end of single_comment
        html_text += '</div>';   //end of comment_box
        $(html_text).insertAfter(".number_of_comments");

        // Set the text box value to empty string
        $("#txt_write_comment").val("");

        // Increase by 1 the number of comments_counter
        comments_counter++;
        $(".number_of_comments").text(comments_counter + " Comments");

      }
      else {
        $("#write_comment_box .btn-send").css("background-image", "url(assets/images/icons/error.png)");
        $("#write_comment_box .btn-send").css("width", "35px");
        $("#txt_write_comment").css("color", "#E43B40");
        $("#txt_write_comment").val("Ooh man! Just write your comment here!");
      }

    });

    $("#txt_write_comment").click(function() {
      $(".comment_box .btn-send").removeAttr( 'style' );
      $("#txt_write_comment").val("");
      $("#txt_write_comment").removeAttr( 'style' );
    });

    // Replay on comment

    $(".all_comments").on("click", ".btn-replay", function(){
        $("#replay_comment_box").css("display", "inline-block");
        $("#replay_comment_box").insertAfter($(this).closest(".comment_box"));
    });

    $("#replay_comment_box .btn-send").click(function() {

      if( $("#txt_replay_comment").val() != "" && $("#txt_replay_comment").val() !="Ooh man! Just write your comment here!") {

        // Post the comment on the page
        var html_text = "";
        html_text += '<div class="comment_box replied_comment">';
        html_text += '<div class="single_comment">';
        html_text += '<div class="comment_header">';
        html_text += '<img src="assets/images/avatar/avatar-1.png" />';
        html_text += '<div class="name_and_time">';
        html_text += '<div class="name">Alfred Henderson</div>';
        html_text += '<div class="time">4 days ago</div></div>';  // end of name_and_time
        html_text += '</div>';      //end of comment_header
        html_text += '<div class="comment_text">';
        html_text += $("#txt_replay_comment").val();
        html_text += '</div>';
        html_text += '</div>';   //end of single_comment
        html_text += '</div>';   //end of comment_box
        $(html_text).insertAfter("#replay_comment_box");

        // Set the text box value to empty string
        $("#txt_replay_comment").val("");
        // Remove the added style to hide the replay comment boc
        $("#replay_comment_box").removeAttr( 'style' );

        // Increase by 1 the number of comments_counter
        comments_counter++;
        $(".number_of_comments").text(comments_counter + " Comments");

      }
      else {
        $("#replay_comment_box .btn-send").css("background-image", "url(assets/images/icons/error.png)");
        $("#replay_comment_box .btn-send").css("width", "35px");
        $("#txt_replay_comment").css("color", "#E43B40");
        $("#txt_replay_comment").val("Ooh man! Just write your comment here!");
      }

    });

    $("#txt_replay_comment").click(function() {
      $(".comment_box .btn-send").removeAttr( 'style' );
      $("#txt_replay_comment").val("");
      $("#txt_replay_comment").removeAttr( 'style' );
    });

    $(".menu-link").click(function() {
      $("ul.menu li").removeClass("active");
      $(this).parent().addClass("active");
    })

    $(window).scroll(function() {
      if ($(window).width() < 768) {
        $(".fixed").css( "background-color", "#fff" );
        $(".fixed").css( "border-bottom", "1px solid #E3E3E3" );
        $(".mobile-menu").css( "background-image", "url(assets/images/icons/menu-black.png)" );
        if($(this).scrollTop() == 0) {
          $(".fixed").removeAttr("style");
          $(".mobile-menu").css( "background-image", "url(assets/images/icons/Menu.png)" );
        }
      }



    });

});
